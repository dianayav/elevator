package service;

import model.Elevator;
import model.Person;

import java.util.List;

public class ElevatorService {
    final List<Elevator> elevators;

    public ElevatorService(List<Elevator> elevators) {
        this.elevators = elevators;
    }

    public Elevator callElevator(Person person) {
        int shortest = Math.abs(person.getPosition().getCurrent() - elevators.get(0).getPosition().getCurrent());
        int index = 0;
        
        for (int i = 1; i < elevators.size(); i++) {
            if (shortest > Math.abs(person.getPosition().getCurrent() - elevators.get(i).getPosition().getCurrent())) {
                shortest = Math.abs(person.getPosition().getCurrent() - elevators.get(i).getPosition().getCurrent());
                index = i;
            }
        }
        return elevators.get(index);
    }
}
