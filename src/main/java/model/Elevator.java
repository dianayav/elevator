package model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Elevator {
    private Position position;
    private String name;

    public Elevator(Position position, String name) {
        this.position = position;
        this.name = name;
    }
}
