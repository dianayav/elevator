package model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Data
@NoArgsConstructor
public class Person {
    private int id;
    private Position position;

    final Logger logger = LogManager.getLogger(Person.class);

    public Person(int id, Position position) {
        this.id = id;
        this.position = position;
    }
}
