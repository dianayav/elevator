package main;

import model.Elevator;
import model.Person;
import model.Position;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.ElevatorService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static validation.Validation.isValidateFloors;

public class Main {
    private static boolean validatePositions(List<Position> positions) {
        for (Position p : positions)
            if (!isValidateFloors(p))
                return false;
        return true;
    }

    public static void main(String[] args) {
        final Logger logger = LogManager.getLogger(Main.class);

        Elevator elevator1 = new Elevator(new Position(12, 10), "Lift 1");
        Elevator elevator2 = new Elevator(new Position(8, 9), "Lift 2");
        Person person1 = new Person(1, new Position(10, 50));

        List<Position> positions = new ArrayList<>(Arrays.asList(elevator1.getPosition(), elevator2.getPosition(), person1.getPosition()));
        boolean isValidPos = validatePositions(positions);

        if (isValidPos) {
            final ElevatorService elevatorService1 = new ElevatorService(new ArrayList<>(Arrays.asList(elevator1, elevator2)));
            String result = String.format("%s -> %s", person1.getId(), elevatorService1.callElevator(person1).getName());
            logger.info(result);
        }
    }
}
