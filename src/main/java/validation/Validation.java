package validation;

import model.Position;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Validation {
    public static final int COUNT_OF_FLOORS = 20;

    private Validation() {}

    public static boolean isValidateFloors(Position position) {
        final Logger logger = LogManager.getLogger(Validation.class);
        if (position.getCurrent() < 1 || position.getCurrent() > COUNT_OF_FLOORS
            || position.getDestination() < 1 || position.getDestination() > COUNT_OF_FLOORS) {
            String logInfo = "Error. Floors must be [1-20]";
            logger.error(logInfo, new IndexOutOfBoundsException().getMessage());
        } else if (position.getCurrent() == position.getDestination()) {
            String logInfo = "You are already on" + position.getDestination() + " F.";
            logger.error(logInfo, new IndexOutOfBoundsException().getMessage());
        } else {
            return true;
        }
        return false;
    }
}
