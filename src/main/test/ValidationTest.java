import model.Elevator;
import model.Person;
import model.Position;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;
import static validation.Validation.isValidateFloors;

public class ValidationTest {
    private final Elevator elevator1 = new Elevator(new Position(-1, 3), "Lift1");
    private final Elevator elevator2 = new Elevator(new Position(19, 30), "Lift2");
    private final Elevator elevator3 = new Elevator(new Position(3, 17), "Lift3");
    private final Elevator elevator4 = new Elevator(new Position(3, 3), "Lift4");

    private final Person person1 = new Person(1, new Position(-4, 10));
    private final Person person2 = new Person(2, new Position(19, 30));
    private final Person person3 = new Person(3, new Position(19, 19));
    private final Person person4 = new Person(4, new Position(1, 20));


    @DataProvider(name = "dataElevator1")
    public Object[] dataElevator1() {
        return new Elevator[]{elevator1, elevator2, elevator4};
    }

    @DataProvider(name = "dataElevator2")
    public Object[] dataElevator2() {
        return new Elevator[]{elevator3};
    }

    @DataProvider(name = "dataPerson1")
    public Object[] dataPerson1() {
        return new Person[]{person3, person2, person1};
    }

    @DataProvider(name = "dataPerson2")
    public Object[] dataPerson2() {
        return new Person[]{person4};
    }

    @Test(dataProvider = "dataElevator1")
    public void testElevator1(Elevator[] elevators) {
        for (Elevator elevator : elevators) {
            assertFalse(isValidateFloors(elevator.getPosition()));
        }
    }

    @Test(dataProvider = "dataElevator2")
    public void testElevator2(Elevator elevator) {
        assertTrue(isValidateFloors(elevator.getPosition()));
    }

    @Test(dataProvider = "dataPerson1")
    public void testPerson1(Person[] people) {
        for (Person person : people) {
            assertFalse(isValidateFloors(person.getPosition()));
        }
    }

    @Test(dataProvider = "dataPerson2")
    public void testPerson2(Person person) {
        assertTrue(isValidateFloors(person.getPosition()));
    }
}
