import model.Elevator;
import model.Person;
import model.Position;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import service.ElevatorService;

import java.util.Arrays;
import java.util.List;

import static org.testng.Assert.assertEquals;

public class ElevatorServiceTest {
    private final Elevator elevator1 = new Elevator(new Position(1, 3), "Lift1");
    private final Elevator elevator2 = new Elevator(new Position(19, 10), "Lift2");
    private final Elevator elevator3 = new Elevator(new Position(3, 17), "Lift3");
    private final Elevator elevator4 = new Elevator(new Position(10, 13), "Lift4");
    private final Elevator elevator5 = new Elevator(new Position(20, 18), "Lift5");
    private final Person person1 = new Person(1, new Position(4, 10));
    private final Person person6 = new Person(2, new Position(19, 3));
    private final Person person7 = new Person(3, new Position(10, 2));
    private final Person person4 = new Person(4, new Position(1, 6));
    private final Person person5 = new Person(5, new Position(10, 12));
    private final List<Elevator> elevators1 = Arrays.asList(elevator1, elevator2, elevator3, elevator4, elevator5);

    private final Elevator elevator6 = new Elevator(new Position(12, 10), "Lift 1");
    private final Elevator elevator7 = new Elevator(new Position(8, 9), "Lift 2");
    private final Person person2 = new Person(1, new Position(9, 5));
    private final List<Elevator> elevators2 = Arrays.asList(elevator6, elevator7);

    private final Elevator elevator8 = new Elevator(new Position(12, 10), "Lift 1");
    private final Elevator elevator9 = new Elevator(new Position(7, 10), "Lift 2");
    private final Person person3 = new Person(2, new Position(10, 12));
    private final List<Elevator> elevators3 = Arrays.asList(elevator8, elevator9);


    @DataProvider(name = "data1")
    public Object[][] getData1() {
        return new Object[][]{
                {elevators1, person1}};
    }

    @DataProvider(name = "data4")
    public Object[][] getData4() {
        return new Object[][]{
                {elevators1, person4}};
    }

    @DataProvider(name = "data5")
    public Object[][] getData5() {
        return new Object[][]{
                {elevators1, person5}};
    }

    @DataProvider(name = "data6")
    public Object[][] getData6() {
        return new Object[][]{
                {elevators1, person6}};
    }

    @DataProvider(name = "data7")
    public Object[][] getData7() {
        return new Object[][]{
                {elevators1, person7}};
    }

    @DataProvider(name = "data2")
    public Object[][] getData2() {
        return new Object[][]{
                {elevators2, person2}};
    }

    @DataProvider(name = "data3")
    public Object[][] getData3() {
        return new Object[][]{
                {elevators3, person3}};
    }

    @Test(dataProvider = "data1")
    public void test1(List<Elevator> elevators, Person person) {
        ElevatorService elevatorService1 = new ElevatorService(elevators);
        assertEquals(elevator3, elevatorService1.callElevator(person));
    }

    @Test(dataProvider = "data6")
    public void test4(List<Elevator> elevators, Person person) {
        ElevatorService elevatorService1 = new ElevatorService(elevators);
        assertEquals(elevator2, elevatorService1.callElevator(person));
    }

    @Test(dataProvider = "data5")
    public void test5(List<Elevator> elevators, Person person) {
        ElevatorService elevatorService1 = new ElevatorService(elevators);
        assertEquals(elevator4, elevatorService1.callElevator(person));
    }

    @Test(dataProvider = "data4")
    public void test6(List<Elevator> elevators, Person person) {
        ElevatorService elevatorService1 = new ElevatorService(elevators);
        assertEquals(elevator1, elevatorService1.callElevator(person));
    }

    @Test(dataProvider = "data7")
    public void test7(List<Elevator> elevators, Person person) {
        ElevatorService elevatorService1 = new ElevatorService(elevators);
        assertEquals(elevator4, elevatorService1.callElevator(person));
    }

    @Test(dataProvider = "data2")
    public void test2(List<Elevator> elevators, Person person) {
        ElevatorService elevatorService2 = new ElevatorService(elevators);
        assertEquals(elevator7, elevatorService2.callElevator(person));
    }

    @Test(dataProvider = "data3")
    public void test3(List<Elevator> elevators, Person person) {
        ElevatorService elevatorService3 = new ElevatorService(elevators);
        assertEquals(elevator8, elevatorService3.callElevator(person));
    }
}
